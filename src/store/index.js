import Vue from "vue";
import Vuex from "vuex";

import { DASHBOARD_CARD_ARR } from "@/constants";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    drawer: true,
    user: null,
    appLoading: false,
    // ----
    statsLoading: false,
    stats: [],
    formattedStats: [],
    statsCount: 0,
  },
  mutations: {
    SET_DRAWER(state, payload) {
      state.drawer = payload;
    },
    SET_USER(state, payload) {
      state.user = payload;
    },
    SET_APP_LOADING(state, payload) {
      state.appLoading = payload;
    },
    SET_STATS_LOADING(state, payload) {
      state.statsLoading = payload;
    },
    SET_STATS(state, { stats, count }) {
      state.stats = stats;
      state.statsCount = count;
      state.formattedStats = stats
        .reduce((acc, current) => {
          const { status } = current;
          const index = acc.findIndex((a) => a.title === status);
          if (status === 0) return acc;
          if (index !== -1) {
            acc[index] = {
              ...acc[index],
              value: acc[index].value + 1,
            };
          } else {
            acc.push({
              title: status,
              value: 1,
              percentage: 6,
              up: true,
            });
          }
          return acc;
        }, [])
        .sort((a, b) => {
          if (a.value > b.value) {
            return -1;
          }
          if (a.value < b.value) {
            return 1;
          }
          return 0;
        });
    },
  },

  getters: {
    DASHBOARD_CARDS(state) {
      return DASHBOARD_CARD_ARR.map((card) => {
        let value = 0;

        const index = state.formattedStats.findIndex(
          (a) => a.title.toLowerCase() === card.title.toLowerCase()
        );

        if (index !== -1) {
          value = state.formattedStats[index].value;
        }

        return {
          ...card,
          value,
        };
      });
    },
    FUNNEL_DATA_SET(state) {
      let funnelLabels = [
        "new",
        "shortlisted",
        "schedule interview",
        "pending decision",
        "job offered",
      ];

      return funnelLabels
        .map((funnel) => {
          const index = state.formattedStats.findIndex(
            (f) => f.title.toLowerCase() == funnel.toLowerCase()
          );
          let value = 0;
          if (index !== -1) {
            value = state.formattedStats[index].value;
          }

          let label = funnel;
          let split = label.split(" ");
          if (split.length > 1) label = split.join("\n");

          return {
            name: label,
            value,
          };
        })
        .sort((a, b) => {
          if (a.value > b.value) {
            return -1;
          }
          if (a.value < b.value) {
            return 1;
          }
          return 0;
        });
    },
  },
  actions: {
    async FETCH_USER({ commit }) {
      try {
        const resp = await Vue.axios.post("get/user", {
          token: localStorage.getItem("token"),
        });

        const user = resp.data.data;
        commit("SET_USER", user);
        return;
      } catch (error) {
        console.log(error);
        return;
      }
    },

    async FETCH_STATS({ commit, state }) {
      try {
        commit("SET_STATS_LOADING", true);
        const { company_id } = state.user;
        const params = {
          whereHas: {
            key: "jobPost",
            value: "company_id," + company_id,
          },
          company_id: company_id,
        };
        const resp = await Vue.axios.get("application/stats", params);
        const { data, count } = resp.data;
        commit("SET_STATS", { stats: data, count });
        commit("SET_STATS_LOADING", false);
      } catch (error) {
        console.log(error);
        commit("SET_STATS_LOADING", false);
        return;
      }
    },
  },
  modules: {},
});
