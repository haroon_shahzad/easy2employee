import Vue from "vue";
import axios from "axios";
import { API_END_POINT } from "@/config";

const service = axios.create({
  baseURL: API_END_POINT,
});

service.interceptors.response.use(
  (response) => {
    let token = response.data.token;
    if (token) {
      localStorage.setItem("token", token);
    }
    return response;
  },
  (error) => {
    const errorMsg = error.response.data.msg;

    Vue.notify({
      group: "backend-error",
      title: "Error",
      text: errorMsg || "Something went wrong...!",
      type: "error",
      duration: 5000,
    });

    localStorage.removeItem("token");
    return Promise.reject(error);
  }
);

service.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("token");
    if (token) {
      config.headers.Authorization = token;
    }
    return config;
  },
  (error) => {
    Vue.notify({
      group: "backend-error",
      title: "Error",
      text: error.response.data.msg || "Something went wrong...!",
      type: "error",
      duration: 5000,
    });

    localStorage.removeItem("token");
    return Promise.reject(error);
  }
);

export default service;
