import Vue from "vue";
import VueAxios from "vue-axios";
import axios from "@/axios";
import Notifications from "vue-notification";
import velocity from "velocity-animate";

import Card from "@/components/card/card";
import Loader from "@/components/loader/loader";

Vue.use(VueAxios, axios);
Vue.use(Notifications, { velocity });
Vue.component("Card", Card);
Vue.component("Loader", Loader);
