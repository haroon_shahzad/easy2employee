import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);
export default new Vuetify({
    theme:{
        themes:{
            light:{
                primary: '#2e3f51',
                secondary: '#33aada',
                accent: '#2E3F51',
                error: '#EF4B35',
                info: '#90278F',
                success: '#4CAF50',
                warning: '#FFC107',
              }
        }
    }
});
