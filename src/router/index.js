import Vue from "vue";
import VueRouter from "vue-router";
import Store from "@/store";

Vue.use(VueRouter);

const routes = [
  {
    path: "",
    component: () => import("@/layouts/DashboardLayout"),
    children: [
      {
        path: "/",
        name: "Dashboard",
        meta: {
          requiresAuth: true,
          roles: [],
        },
        component: () => import("@/views/Dashboard"),
      },
    ],
  },
  // blank pages route
  {
    path: "",
    component: () => import("@/layouts/BlankLayout"),
    children: [
      {
        path: "/login",
        name: "login",
        meta: {
          requiresAuth: false,
        },
        component: () => import("@/views/auth/Login"),
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach(async (to, from, next) => {
  const { requiresAuth } = to.meta;
  let isAuthenticated = false;

  const token = localStorage.getItem("token");

  if (token) {
    const { user } = Store.state;
    if (!user) {
      try {
        Store.commit("SET_APP_LOADING", true);
        await Store.dispatch("FETCH_USER");
        isAuthenticated = true;
      } catch (error) {
        console.log("{ROUTER INDEX}", error);
      }
      setTimeout(() => {
        Store.commit("SET_APP_LOADING", false);
      }, 2000);
    } else {
      isAuthenticated = true;
    }
  }

  if (requiresAuth && isAuthenticated) {
    next();
  } else if (requiresAuth && !isAuthenticated) {
    next("/login");
  } else if (!requiresAuth && isAuthenticated) {
    next("/");
  } else {
    next();
  }
});

export default router;
